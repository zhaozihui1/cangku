<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Imaget extends Controller
{
    public function getImage(){
        $data = DB::table('img')->get()->toArray();
        return ['code'=>1,'data'=>$data,'msg'=>'传输成功'];
    }
}
